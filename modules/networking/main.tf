resource "aws_vpc" "vpc" {

  cidr_block = var.vpc-cidr
  enable_dns_hostnames = var.vpc-dns

  tags = {
    "Name" = "${var.project-name}-vpc"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  
  tags = {
    "Name" = "${var.project-name}-igw"
  }
}

# resource "aws_internet_gateway_attachment" "igw-attach" {
#   vpc_id = aws_vpc.vpc.id
#   internet_gateway_id = aws_internet_gateway.igw.id
# }

resource "aws_route_table" "pub-route-table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  } 

  tags = {
    "Name" = "${var.project-name}-route-table"
  }
}

resource "aws_subnet" "pub-subnet-1" {
  vpc_id = aws_vpc.vpc.id
  cidr_block = var.pub-subnet-1-cidr
  availability_zone = var.pub-subnet-1-az

  tags = {
    "Name" = "${var.project-name}-pub-subnet-1"
  }
}

resource "aws_route_table_association" "associat-route-table-1" {
  route_table_id = aws_route_table.pub-route-table.id
  subnet_id = aws_subnet.pub-subnet-1.id
}
