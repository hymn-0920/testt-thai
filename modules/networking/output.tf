output "vpc-id" {
  value = aws_vpc.vpc.id
}

output "pub-subnet-id" {
  value = aws_subnet.pub-subnet-1.id
}