variable "vpc-id" { 
}

variable "subnet-id" {
}

variable "project-name" {
  
}
variable "ami-id" {
  type = string
}

variable "instance-type" {
}

variable "pub-ip-instance" {
}

variable "keyPair" {
}