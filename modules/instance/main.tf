resource "aws_security_group" "sg-1" {
  name        = "pub-sg"
  vpc_id      = var.vpc-id

  ingress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "pub-sg"
  }
}
resource "aws_instance" "ec2-instance" {
  ami = var.ami-id
  instance_type = var.instance-type
  associate_public_ip_address = var.pub-ip-instance
  key_name = var.keyPair
  security_groups = [aws_security_group.sg-1.id]
  subnet_id = var.subnet-id
  user_data = <<-EOF
                #!/bin/bash
                apt update -y
                apt install nginx -y
                EOF

  tags = {
    "Name" = "${var.project-name}-ec2"
  }
}

