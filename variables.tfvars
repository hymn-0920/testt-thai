project-name = "test-terraform-2"
## Networking var
vpc-cidr = "10.0.0.0/16"
vpc-dns = true
pub-subnet-1-cidr = "10.0.4.0/24"
pub-subnet-1-az = "sa-east-1a"

## Instance var
ami-id = "ami-04b3c23ec8efcc2d6"
instance-type = "t2.micro"
pub-ip-instance = true
keyPair = "DanhDD1"
