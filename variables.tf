variable "project-name" {
  type = string
}

## Networking var
variable "vpc-cidr" {
  type = string
}

variable "vpc-dns" {
  type = bool
}

variable "pub-subnet-1-cidr" {
  type = string
}

variable "pub-subnet-1-az" {
  type = string
}

##Instance var

variable "ami-id" {
}

variable "instance-type" {
  type = string
}

variable "pub-ip-instance" {
  type = bool
}

variable "keyPair" {
  type = string
}
