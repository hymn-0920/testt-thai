terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 4.0"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  profile = "default"
  region = "sa-east-1"
}

# locals {
#   project = "TerraForm"
# }

module "networking" {
  source = "./modules/networking"

  project-name = var.project-name
  vpc-cidr = var.vpc-cidr
  vpc-dns = var.vpc-dns
  pub-subnet-1-cidr = var.pub-subnet-1-cidr
  pub-subnet-1-az = var.pub-subnet-1-az
}

module "instance" {
  source = "./modules/instance"

  vpc-id = module.networking.vpc-id
  subnet-id = module.networking.pub-subnet-id
  project-name = var.project-name
  ami-id = var.ami-id
  instance-type = var.instance-type
  pub-ip-instance = var.pub-ip-instance
  keyPair = var.keyPair
}
# resource "aws_instance" "test_instance" {
#   ami           = "ami-0f8048fa3e3b9e8ff"
#   instance_type = "t2.micro"
#   associate_public_ip_address = true
#   security_groups = [ "sg-0c96b42897fcfeae7" ]
#   subnet_id = "subnet-0648d76be82b0e2c9"
#   key_name = "DanhDD1"

#   tags = {
#     Name = "HelloWorld"
#   }
# }