output "ec2-pub-ip" {
  value = module.instance.instance-pub-ip
}

output "vpc-id" {
  value = module.networking.vpc-id
}

output "subnet-id" {
  value = module.networking.pub-subnet-id
}

output "Test" {
  value = "Test"
}